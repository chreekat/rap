module Main where

import Pardon

import Data.Maybe
import System.Environment
import qualified Data.Text as T
import qualified Data.Text.IO as T


main = do
    a <- getArgs
    let l = maybe 80 read (listToMaybe a)
    contents <- T.getContents
    let costs = map (cost l . T.length) (T.lines contents)
    print (foldr1 plus costs)
    
