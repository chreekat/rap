.PHONY: all
all: RapProf.ps RapProf.prof

RapProf.ps: RapProf.hp
	hp2ps -c $<
RapProf: Rap.hs
	ghc $< -o $@ -O2 -main-is Rap.profMain -prof -fprof-auto -rtsopts
RapProf.hp: RapProf
	./$< 1 ~/Documents/Log.txt 5 +RTS -hy
RapProf.prof: RapProf
	./$< 1 ~/Documents/Log.txt 5 +RTS -p

RapBench.html: RapBench
	./$< --output $@

RapBench: Rap.hs
	ghc $< -o $@ -O2 -main-is Rap.benchMain -rtsopts

rap: Rap.hs
	ghc $< -o $@ -O2 -main-is Rap.main
